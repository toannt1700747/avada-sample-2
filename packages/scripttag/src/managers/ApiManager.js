import makeRequest from '../helpers/api/makeRequest';

export default class ApiManager {
  getNotifications = async () => {
    return this.getApiData();
  };

  getApiData = async () => {
    const shopifyDomain = window.Shopify.shop;

    const {data} = await makeRequest(
      `https://localhost:3000/clientApi/shop?shopifyDomain=${shopifyDomain}`
    );
    const {notifications, settings} = data;

    return {notifications, settings};
  };
}
