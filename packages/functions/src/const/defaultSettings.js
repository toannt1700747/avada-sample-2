export const defaultSetting = {
  position: 'bottom-left',
  hideTimeAgo: false,
  truncateProductName: false,
  displayDuration: 2,
  firstDelay: 3,
  popsInterval: 3,
  maxPopsDisplay: 5,
  includedUrls: '',
  excludedUrls: '',
  allowShow: 'all'
};
