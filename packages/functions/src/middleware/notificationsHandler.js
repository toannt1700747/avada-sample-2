import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();
const collection = firestore.collection('notifications');

export default async function notifcationsHandler(ctx, next) {
  try {
    const orderData = ctx.req.body;

    const docs = await collection
      .where('orderId', '==', orderData.id)
      .limit(1)
      .get();

    if (!docs.empty) {
      return (ctx.body = {
        success: false
      });
    }
    return next();
  } catch (error) {
    console.error(error);
    return (ctx.body = {
      success: false
    });
  }
}
