import {Firestore} from '@google-cloud/firestore';
import {defaultSetting} from '../const/defaultSettings';

const firestore = new Firestore();
const collection = firestore.collection('settings');

export async function addDefaultSetting(shopId) {
  try {
    return collection.add({...defaultSetting, shopId});
  } catch (e) {
    console.error(e);
    return null;
  }
}
export async function getSetting(shopId) {
  try {
    const settingDos = await collection
      .where('shopId', '==', shopId)
      .limit(1)
      .get();
    if (settingDos.empty) {
      return null;
    }

    const [settingDoc] = settingDos.docs;
    return {id: settingDoc.id, ...settingDoc.data()};
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function updateSetting(setting) {
  try {
    return collection.doc(setting.id).update(setting);
  } catch (e) {
    console.error(e);
    return null;
  }
}
