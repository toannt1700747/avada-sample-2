import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();
const collection = firestore.collection('notifications');

export async function getListNotifications({shopId, limit = 5, sort = 'createdAt:desc', page = 1}) {
  try {
    const limitPage = parseInt(limit);
    const numberPage = parseInt(page);

    let hasPre = true,
      hasNext = true;
    if (numberPage === 1) {
      hasPre = false;
    }

    let queryNotification = collection.where('shopId', '==', shopId);
    const timeStamp = sort.split(':')[1];
    const timeStampLast = timeStamp == 'desc' ? 'asc' : 'desc';

    if (sort) {
      queryNotification = queryNotification.orderBy('timestamp', timeStamp);
    }

    if (limitPage) {
      const offsetPage = (numberPage - 1) * limitPage;
      queryNotification = queryNotification.limit(limitPage).offset(offsetPage);
    }

    const notificationDocs = await queryNotification.get();

    if (notificationDocs.empty) {
      return null;
    }
    const notifications = notificationDocs.docs;

    const {docs: notifcationLastDoc} = await collection
      .where('shopId', '==', shopId)
      .orderBy('timestamp', timeStampLast)
      .limit(1)
      .get();

    if (notifications[notifications.length - 1].id === notifcationLastDoc[0].id) {
      hasNext = false;
    }

    const notifcationData = notifications.map(notification => {
      const formatTime = notification.data().timestamp.toDate();
      return {
        id: notification.id,
        ...notification.data(),
        timestamp: formatTime
      };
    });

    return {
      data: notifcationData,
      pageInfo: {
        hasPre,
        hasNext
      }
    };
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function addNotification(notifcation) {
  try {
    return collection.add(notifcation);
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function deleteMassNotification(indexArr = []) {
  const batch = firestore.batch();

  indexArr.forEach(id => batch.delete(collection.doc(id)));

  return batch.commit();
}
