import appConfig from '@functions/config/app';
import {isEmpty} from '@avada/utils';

export async function createWebhook(shopify) {
  const {baseUrl} = appConfig;
  const address = `https://${baseUrl}/webhook/order/new`;

  const webhookList = await shopify.webhook.list();

  const webhookItem = webhookList.filter(webhook => webhook.address === address);

  if (!isEmpty(webhookItem)) {
    return;
  }

  await Promise.all(webhookList.map(webhook => shopify.webhook.delete(webhook.id)));

  return shopify.webhook.create({
    topic: 'orders/create',
    address,
    format: 'json'
  });
}
