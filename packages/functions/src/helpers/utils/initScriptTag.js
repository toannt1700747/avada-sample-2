import {isEmpty} from '@avada/utils';

export async function initScriptTag(shopify) {
  const src = `https://localhost:3000/scripttag/avada-sale-pop.min.js`;

  const scriptTagList = await shopify.scriptTag.list();

  const scriptTagItem = scriptTagList.filter(scriptTag => scriptTag.src === src);

  if (!isEmpty(scriptTagItem)) {
    return;
  }

  await Promise.all(scriptTagList.map(scriptTag => shopify.scriptTag.delete(scriptTag.id)));

  return shopify.scriptTag.create({
    event: 'onload',
    src
  });
}
