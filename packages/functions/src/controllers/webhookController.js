import {getShopByShopifyDomain} from '@avada/shopify-auth';
import {initShopify} from '../helpers/utils/initShopify';
import {addNotification} from '../repositories/notificationsRepository';
import {loadNotification} from '../services/shopifyService';

export async function listenNewOrder(ctx) {
  try {
    const shopifyDomain = ctx.get('X-Shopify-Shop-Domain');
    const shop = await getShopByShopifyDomain(shopifyDomain);

    const orderData = ctx.req.body;

    const shopify = initShopify(shop);

    const productId = orderData?.line_items[0]?.product_id;
    const product = await shopify.product.get(productId);

    const noticationData = loadNotification({shop, order: orderData, product});

    await addNotification(noticationData);

    return (ctx.body = {
      success: true
    });
  } catch (error) {
    console.error(error);
    return (ctx.body = {
      success: false
    });
  }
}
