import {getCurrentShop} from '../helpers/auth';
import {
  deleteMassNotification,
  getListNotifications
} from '../repositories/notificationsRepository';

export async function getListNotificationsData(ctx) {
  const shopId = getCurrentShop(ctx);
  const {limit, sort, page} = ctx.query;

  const noticationData = await getListNotifications({shopId, limit, sort, page});

  ctx.body = {
    data: noticationData?.data,
    pageInfo: noticationData?.pageInfo,
    success: true
  };
}

export async function deleteMassNotificationData(ctx) {
  try {
    const {data: indexArr} = ctx.req.body;

    await deleteMassNotification(indexArr);

    ctx.body = {
      success: true
    };
  } catch (error) {
    console.log(error);
    ctx.body = {
      success: false
    };
  }
}
