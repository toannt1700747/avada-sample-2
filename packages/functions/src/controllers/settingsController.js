import {getCurrentShop} from '../helpers/auth';
import {getSetting, updateSetting} from '../repositories/settingsRepository';

export async function getSettingData(ctx) {
  const shopId = getCurrentShop(ctx);
  const settingData = await getSetting(shopId);
  ctx.body = {data: settingData, success: true};
}

export async function updateSettingData(ctx) {
  const {data} = ctx.req.body;
  await updateSetting(data);

  ctx.body = {success: true};
}
