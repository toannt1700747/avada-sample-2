import {getShopByShopifyDomain} from '@avada/shopify-auth';
import {resolveAll} from '../helpers/utils/resolveAll';
import {getListNotifications} from '../repositories/notificationsRepository';
import {getSetting} from '../repositories/settingsRepository';

export async function dataClient(ctx) {
  try {
    const {shopifyDomain} = ctx.query;
    const shop = await getShopByShopifyDomain(shopifyDomain);

    const [settings, notifications] = await resolveAll([
      getSetting(shop.id),
      getListNotifications({shopId: shop.id, limit: 30})
    ]);

    return (ctx.body = {
      data: {
        settings,
        notifications: notifications?.data
      },
      success: true
    });
  } catch (error) {
    console.error(error);
    return (ctx.body = {
      success: false
    });
  }
}
