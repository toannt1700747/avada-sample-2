import axios from 'axios';
import {resolveAll} from '../helpers/utils/resolveAll';
import {addNotification} from '../repositories/notificationsRepository';
import {convertToId} from '../helpers/utils/convertToId';

/**
 *
 * @param {*} shop
 */
export async function pushNotification(shop) {
  const {id: shopId, shopifyDomain, accessToken} = shop;
  const url = `https://${shopifyDomain}/admin/api/2023-10/graphql.json`;

  try {
    const {data: resultData} = await axios.post(
      url,
      {
        query: `
        {
          orders(first: 30, sortKey:CREATED_AT, reverse:true) {
            edges {
              node {
               id
               shippingAddress {
                 firstName
                 city
                 country
               }
               createdAt
                lineItems(first:1){
                  edges{
                    node{
                      image{
                        url
                      }
                      product{
                        id
                        title
                      }
                    }
                  }
                }
              }
            }
          }
        }
         `
      },
      {
        headers: {
          'X-Shopify-Access-Token': accessToken
        }
      }
    );

    const ordersData = resultData.data.orders.edges || [];

    const notificationList = ordersData.map(order => {
      const orderData = order.node;
      const shippingAddress = orderData.shippingAddress;
      const lineItem = orderData.lineItems.edges[0].node || {};
      return {
        orderId: convertToId(orderData.id),
        firstName: shippingAddress.firstName,
        city: shippingAddress.city,
        productName: lineItem.product.title,
        country: shippingAddress.country,
        productId: convertToId(lineItem.product.id),
        timestamp: new Date(orderData.createdAt),
        productImage: lineItem.image.url,
        shopId,
        shopifyDomain
      };
    });

    await resolveAll(notificationList.map(notification => addNotification(notification)));
  } catch (error) {
    console.error(error);
  }
}
