import {getShopByShopifyDomain} from '@avada/shopify-auth';
import {initShopify} from '../helpers/utils/initShopify';
import {resolveAll} from '../helpers/utils/resolveAll';
import {addDefaultSetting} from '../repositories/settingsRepository';
import {pushNotification} from './shopifyService';
// import {pushNotification} from './shopifyGraphqlService';
import {createWebhook} from '../helpers/utils/createWebhook';
import {initScriptTag} from '../helpers/utils/initScriptTag';

export async function afterInstall(ctx) {
  try {
    const shopifyDomain = ctx.state.shopify.shop;
    const shop = await getShopByShopifyDomain(shopifyDomain);

    const shopify = initShopify(shop);

    await resolveAll([
      pushNotification(shopify, shop),
      //   // pushNotification(shop) // grapql
      addDefaultSetting(shop.id),
      initScriptTag(shopify),
      createWebhook(shopify)
    ]);
  } catch (e) {
    console.error(e);
  }
}
