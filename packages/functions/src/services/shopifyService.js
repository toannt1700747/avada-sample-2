import {resolveAll} from '../helpers/utils/resolveAll';
import {addNotification} from '../repositories/notificationsRepository';

export function loadNotification({shop, order, product}) {
  const shippingAddress = order?.shipping_address;
  if (!shippingAddress) return {};

  return {
    orderId: order.id,
    firstName: shippingAddress.first_name,
    city: shippingAddress.city,
    productName: product.title,
    country: shippingAddress.country,
    productId: product.id,
    timestamp: new Date(order.created_at),
    productImage: product.image.src,
    shopId: shop.id,
    shopifyDomain: shop.shopifyDomain
  };
}

export async function pushNotification(shopify, shop) {
  const orders = await shopify.order.list({limit: 30});

  const productIdArray = orders.map(order => {
    return order.line_items[0].product_id;
  });

  const products = await shopify.product.list({ids: [...new Set(productIdArray)].toString()});

  const notifications = orders.map(order => {
    const product = products.find(product => product.id === order.line_items[0].product_id);
    if (!product) return {};

    return loadNotification({shop, order, product});
  });

  await resolveAll(
    notifications.map(notification => {
      return addNotification(notification);
    })
  );
}
