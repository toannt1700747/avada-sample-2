import Router from 'koa-router';
import * as webhookController from '../controllers/webhookController';
import notifcationsHandler from '../middleware/notificationsHandler';

export default function webhookRouter() {
  const router = new Router({prefix: '/webhook'});

  router.post('/order/new', notifcationsHandler, webhookController.listenNewOrder);
  return router;
}
