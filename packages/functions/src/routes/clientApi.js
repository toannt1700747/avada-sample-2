import Router from 'koa-router';
import * as clientApiController from '../controllers/clientApiController';

export default function clientApiRouter() {
  const router = new Router({prefix: '/clientApi'});

  router.get('/shop', clientApiController.dataClient);
  return router;
}
