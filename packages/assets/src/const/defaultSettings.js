export const defaultSetting = {
  position: 'bottom-left',
  hideTimeAgo: false,
  truncateProductName: true,
  displayDuration: 3,
  firstDelay: 5,
  popsInterval: 2,
  maxPopsDisplay: 10,
  includedUrls: '',
  excludedUrls: '',
  allowShow: 'all'
};
