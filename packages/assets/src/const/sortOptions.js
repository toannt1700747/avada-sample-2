export const sortOptions = [
  {label: 'Newest update', value: 'createdAt:desc'},
  {label: 'Oldest update', value: 'createdAt:asc'}
];
