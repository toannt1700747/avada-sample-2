import {
  Card,
  EmptyState,
  FormLayout,
  Layout,
  Page,
  Pagination,
  ResourceItem,
  ResourceList,
  Stack,
  TextStyle
} from '@shopify/polaris';
import React, {useState} from 'react';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import {sortOptions} from '../../const/sortOptions';
import useFetchApi from '../../hooks/api/useFetchApi';
import moment from 'moment';
import usePaginate from '../../hooks/api/usePaginate';
import useDeleteApi from '../../hooks/api/useDeleteApi';

export default function Notifications() {
  const [sortValue, setSortValue] = useState('createdAt:desc');
  const [selectedItems, setSelectedItems] = useState([]);
  const {data: settingValue} = useFetchApi({
    url: '/settings'
  });

  const {
    prevPage,
    nextPage,
    data: items,
    loading,
    setLoading,
    pageInfo,
    onQueryChange
  } = usePaginate({
    url: '/notifications'
  });

  const {deleting, handleDelete} = useDeleteApi({url: '/notifications'});

  const {hasPre, hasNext} = pageInfo;
  const promotedBulkActions = [
    {
      content: 'Delete',
      onAction: async () => {
        await handleDelete(selectedItems);
        await onQueryChange('page', 1, true);
        setSelectedItems([]);
      }
    }
  ];

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  return (
    <Page title="Notifications" subtitle="List of sales notifcation from Shopify" fullWidth>
      <Layout sectioned>
        <Layout.Section>
          <Card>
            <ResourceList
              resourceName={resourceName}
              items={items}
              renderItem={renderItem}
              selectedItems={selectedItems}
              sortValue={sortValue}
              sortOptions={sortOptions}
              onSortChange={selected => {
                setSortValue(selected);
                onQueryChange('sort', selected, true);
              }}
              onSelectionChange={setSelectedItems}
              promotedBulkActions={promotedBulkActions}
              loading={loading}
              emptyState={
                <EmptyState
                  heading="Upload a file"
                  action={{content: 'Upload files'}}
                  image="https://cdn.shopify.com/s/files/1/2376/3301/products/emptystate-files.png"
                />
              }
            />
          </Card>
        </Layout.Section>
        {items.length !== 0 && (
          <Layout.Section>
            <Stack distribution="center">
              <Pagination
                hasPrevious={hasPre}
                onPrevious={prevPage}
                hasNext={hasNext}
                onNext={nextPage}
              />
            </Stack>
          </Layout.Section>
        )}
      </Layout>
    </Page>
  );

  function renderItem(item) {
    const {id, firstName, city, country, productName, timestamp, productImage} = item;
    return (
      <ResourceItem id={id}>
        <Stack distribution="equalSpacing" alignment="leading">
          <NotificationPopup
            firstName={firstName}
            city={city}
            country={country}
            productName={productName}
            timestamp={timestamp}
            productImage={productImage}
            settings={settingValue}
          />
          <Stack vertical alignment="trailing" spacing="tight">
            <TextStyle>From {moment(timestamp).format('MMM D')},</TextStyle>
            <TextStyle>{moment(timestamp).format('YYYY')}</TextStyle>
          </Stack>
        </Stack>
      </ResourceItem>
    );
  }
}
