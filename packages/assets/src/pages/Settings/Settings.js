import {Card, Layout, Page, SkeletonBodyText, Tabs} from '@shopify/polaris';
import React, {useCallback, useState} from 'react';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import Display from '../../components/Display/Display';
import Triggers from '../../components/Triggers/Triggers';
import {defaultSetting} from '../../const/defaultSettings';
import useEditApi from '../../hooks/api/useEditApi';
import useFetchApi from '../../hooks/api/useFetchApi';

/**
 * @return {JSX.Element}
 */

export default function Settings() {
  const [seletedTab, setSelectedTab] = useState(0);
  const {data: settingValue, loading, handleChangeInput: hanldeSettingValue} = useFetchApi({
    url: '/settings',
    defaultData: defaultSetting
  });

  const {editing, handleEdit} = useEditApi({url: '/settings'});

  const tabs = [
    {
      id: 1,
      content: 'Display',
      title: 'APPEARANCE',
      contentBody: <Display settingValue={settingValue} hanldeSettingValue={hanldeSettingValue} />
    },
    {
      id: 2,
      content: 'Triggers',
      title: 'PAGES RESTRICTION',
      contentBody: <Triggers settingValue={settingValue} hanldeSettingValue={hanldeSettingValue} />
    }
  ];

  const handleTabChange = useCallback(selectedTabIndex => setSelectedTab(selectedTabIndex), []);

  if (loading)
    return (
      <Page title="Settings" subtitle="Decide how your notifications will display" fullWidth>
        <Layout>
          <Layout.Section oneThird>
            <Card sectioned>
              <SkeletonBodyText lines={5} />
            </Card>
          </Layout.Section>
          <Layout.Section>
            <Card sectioned>
              <SkeletonBodyText lines={10} />
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    );

  return (
    <Page
      title="Settings"
      subtitle="Decide how your notifications will display"
      fullWidth
      divider
      primaryAction={{
        content: 'Save',
        onAction: () => {
          handleEdit(settingValue);
        },
        loading: editing
      }}
    >
      <Layout>
        <Layout.Section oneThird>
          <NotificationPopup settings={settingValue} />
        </Layout.Section>
        <Layout.Section>
          <Card>
            <Tabs tabs={tabs} selected={seletedTab} onSelect={handleTabChange}>
              {tabs[seletedTab].contentBody}
            </Tabs>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
Settings.propTypes = {};
