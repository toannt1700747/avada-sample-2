import React from 'react';
import {Card, FormLayout, Select, Stack, TextField} from '@shopify/polaris';

const Triggers = ({settingValue, hanldeSettingValue}) => {
  const {excludedUrls, includedUrls, allowShow} = settingValue;
  const pageOptions = [
    {
      label: 'All pages',
      value: 'all',
      contentbody: (
        <TextField
          label="Exclude pages"
          multiline={4}
          helpText="Page URLs NOT to show the pop-up (seperated by new lines)"
          value={excludedUrls}
          onChange={val => hanldeSettingValue('excludedUrls', val)}
          autoComplete="off"
        />
      )
    },
    {
      label: 'Specific pages',
      value: 'specific',
      contentbody: (
        <Stack vertical>
          <TextField
            label="Include pages"
            multiline={4}
            helpText="Page URLs to show the pop-up (seperated by new lines)"
            value={includedUrls}
            onChange={val => hanldeSettingValue('includedUrls', val)}
            autoComplete="off"
          />
          <TextField
            label="Exclude pages"
            multiline={4}
            helpText="Page URLs NOT to show the pop-up (seperated by new lines)"
            value={excludedUrls}
            onChange={val => hanldeSettingValue('excludedUrls', val)}
            autoComplete="off"
          />
        </Stack>
      )
    }
  ];
  return (
    <Card.Section title="PAGES RESTRICTION">
      <FormLayout>
        <Select
          options={pageOptions}
          onChange={val => hanldeSettingValue('allowShow', val)}
          value={allowShow}
        />
        {pageOptions.find(page => page.value == allowShow).contentbody}
      </FormLayout>
    </Card.Section>
  );
};

export default Triggers;
