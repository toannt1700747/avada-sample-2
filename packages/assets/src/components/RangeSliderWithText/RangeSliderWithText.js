import {RangeSlider, TextField} from '@shopify/polaris';
import PropTypes from 'prop-types';
import React from 'react';

const RangeSliderWithText = ({label, helpText, value, onChange, suffix = 'second(s)'}) => {
  return (
    <RangeSlider
      suffix={
        <div style={{width: '110px'}}>
          <TextField
            value={value.toString()}
            suffix={suffix}
            autoComplete="off"
            onChange={onChange}
          />
        </div>
      }
      label={label}
      value={value}
      onChange={onChange}
      helpText={helpText}
      output
    />
  );
};

RangeSliderWithText.propTypes = {
  label: PropTypes.string,
  helpText: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func
};

export default RangeSliderWithText;
