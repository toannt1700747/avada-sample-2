import React from 'react';
import {Card, Checkbox, FormLayout} from '@shopify/polaris';
import DesktopPositionInput from '../DesktopPositionInput/DesktopPositionInput';
import RangeSliderWithText from '../RangeSliderWithText/RangeSliderWithText';

const Display = ({settingValue, hanldeSettingValue}) => {
  return (
    <>
      <Card.Section title="APPEARANCE">
        <FormLayout>
          <DesktopPositionInput
            label={'Desktop Position'}
            value={settingValue.position}
            onChange={val => hanldeSettingValue('position', val)}
            helpText="The display positon of the pop on your website"
          />
          <Checkbox
            label="Hide time ago"
            onChange={val => hanldeSettingValue('hideTimeAgo', val)}
            checked={settingValue.hideTimeAgo}
          />
          <Checkbox
            label="Truncate content text"
            onChange={val => {
              hanldeSettingValue('truncateProductName', val);
            }}
            checked={settingValue.truncateProductName}
            helpText="If your product name is long for on line, it will be truncated to 'Product na..."
          />
        </FormLayout>
      </Card.Section>

      <Card.Section title="TIMING">
        <FormLayout>
          <FormLayout.Group>
            <RangeSliderWithText
              label="Display duration"
              helpText="How long each pop will display on your page"
              value={settingValue.displayDuration}
              onChange={val => {
                hanldeSettingValue('displayDuration', Number(val));
              }}
            />
            <RangeSliderWithText
              label="Time before the first pop"
              helpText="The delay time before the first notification"
              value={settingValue.firstDelay}
              onChange={val => {
                hanldeSettingValue('firstDelay', Number(val));
              }}
            />
          </FormLayout.Group>
          <FormLayout.Group>
            <RangeSliderWithText
              label="Gap time between two pops"
              helpText="The time interval between two popup notifications"
              value={settingValue.popsInterval}
              onChange={val => {
                hanldeSettingValue('popsInterval', Number(val));
              }}
            />
            <RangeSliderWithText
              label="Maximum of popups"
              helpText="The maximum number of popups are allowed to show after page loading. Maximum number is 80."
              value={settingValue.maxPopsDisplay}
              suffix="pop(s)"
              onChange={val => {
                hanldeSettingValue('maxPopsDisplay', Number(val));
              }}
            />
          </FormLayout.Group>
        </FormLayout>
      </Card.Section>
    </>
  );
};

export default Display;
